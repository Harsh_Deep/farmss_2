package com.tcs.farmss.global;

import android.app.Application;
import android.content.Context;

import com.tcs.farmss.util.LocaleHelper;

/**
 * Created by Harsh on 1/19/2017.
 */
public class MainApplication extends Application {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(LocaleHelper.onAttach(base, "mr"));

    }
}
