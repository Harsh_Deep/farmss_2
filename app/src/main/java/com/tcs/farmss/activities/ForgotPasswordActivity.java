package com.tcs.farmss.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.EditText;

import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.button.ButtonPlus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/19/2017.
 */
public class ForgotPasswordActivity extends AppCompatActivity {

    @Bind(R.id.etForgotPassword)
    EditText etForgotPassword;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnForgotPassword)
    ButtonPlus btnForgotPassword;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.ForgetPassword)));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
