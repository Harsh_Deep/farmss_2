package com.tcs.farmss.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;
import com.tcs.farmss.util.LocaleHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 1/19/2017.
 */
public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.etMobile)
    EditText etMobile;
    @Bind(R.id.etPassword)
    EditText etPassword;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnLogin)
    ButtonPlus btnLogin;
    @Bind(R.id.txtForgotPassword)
    TextView txtForgotPassword;
    @Bind(R.id.txtRegister)
    TextView txtRegister;
    @Bind(R.id.txtChangeLanguage)
    TextView txtChangeLanguage;

    @OnClick(R.id.btnLogin)
    void enter() {
        if (etMobile.getText().toString().equals("") || etPassword.getText().toString().equals("")) {
            Snackbar.show(this, getString(R.string.no_text));
            return;
        }
        else if (!(etMobile.getText().length() == 10)) {
            Snackbar.show(this, getString(R.string.invalid_mobile));
            return;
        }
        else {
            startActivity(new Intent(this, DashboardActivity.class));
            finish();
        }
    }

    @OnClick(R.id.txtForgotPassword)
    void forgotPassword() {
        startActivity(new Intent(this, ForgotPasswordActivity.class));
    }

    @OnClick(R.id.txtChangeLanguage)
    void changeLanguage(){
        startActivity(new Intent(this, ChangeLanguageActivity.class));
    }

    @OnClick(R.id.txtRegister)
    void register(){
        startActivity(new Intent(this, RegisterActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnLogin.setBackgroundResource(R.drawable.ripple);
        }
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.LoginActivityLable)));

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        ActivityCompat.finishAffinity(this);
    }
}
