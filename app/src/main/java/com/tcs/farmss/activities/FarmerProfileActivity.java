package com.tcs.farmss.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Harsh on 1/20/2017.
 */
public class FarmerProfileActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnTakeTest)
    ButtonPlus btnTakeTest;
    @Bind(R.id.txtFarmerName)
    TextView txtFarmerName;
    @Bind(R.id.txtMobileNumber)
    TextView txtMobileNumber;
    @Bind(R.id.txtAddress)
    TextView txtAddress;

    @OnClick(R.id.btnTakeTest)
    void takeTest() {
        new MaterialDialog.Builder(FarmerProfileActivity.this)
                .title(R.string.plot_size).customView(R.layout.enter_plot_size_custom_view, true).inputType(InputType.TYPE_CLASS_NUMBER)
                .positiveText(R.string.ok)
                .typeface("Whitney-Semibold-Bas.otf", "Whitney-Semibold-Bas.otf").contentGravity(GravityEnum.CENTER).onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                View positiveAction = dialog.getActionButton(DialogAction.POSITIVE);
                EditText plotSize = (EditText) dialog.getCustomView().findViewById(R.id.etPlotSize);
                if (plotSize.getText().toString().equals("")) {
                    Snackbar.show(FarmerProfileActivity.this, getString(R.string.plot_size_error));
                    return;
                } else {
                    startActivity(new Intent(FarmerProfileActivity.this, SoilTestActivity.class));
                }
            }
        }).show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_farmer_profile);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.farmer_profile)));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnTakeTest.setBackgroundResource(R.drawable.ripple);
        }

        txtFarmerName.setText(CustomTitle.getTitle(this, getResources().getString(R.string.farmer_name)));
        txtMobileNumber.setText(CustomTitle.getPlainTitle(this, getResources().getString(R.string.mobile_number)));
        txtAddress.setText(CustomTitle.getPlainTitle(this, getResources().getString(R.string.farmer_address)));

    }
}
