package com.tcs.farmss.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tcs.farmss.R;
import com.tcs.farmss.adapters.SoilTestAdapter;
import com.tcs.farmss.constants.TestDTO;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/21/2017.
 */
public class SoilTestActivity extends AppCompatActivity {

    @Bind(R.id.recyclerTest)
    RecyclerView recyclerTest;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.btnNext)
    ButtonPlus btnNext;
    private SoilTestAdapter adapter;
    private List<TestDTO> testDTOs;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soil_test);
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            // Activity was brought to front and not created,
            // Thus finishing this will get us to the last viewed activity
            finish();
            return;
        }
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.tests)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnNext.setBackgroundResource(R.drawable.ripple);
        }

        testDTOs = new ArrayList<>();
        adapter = new SoilTestAdapter(this, testDTOs);

        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 2);
        recyclerTest.setLayoutManager(manager);
        recyclerTest.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerTest.setItemAnimator(new DefaultItemAnimator());
        recyclerTest.setAdapter(adapter);

        prepareTests();
    }

    private void prepareTests() {
        int[] tests = {R.drawable.ph, R.drawable.nitrate, R.drawable.phosphorus, R.drawable.potash_test};

        TestDTO a = new TestDTO("ph Test", tests[0]);
        testDTOs.add(a);

        TestDTO b = new TestDTO("N test", tests[1]);
        testDTOs.add(b);

        TestDTO c = new TestDTO("P Test", tests[2]);
        testDTOs.add(c);

        TestDTO d = new TestDTO("K Test", tests[3]);
        testDTOs.add(d);

        adapter.notifyDataSetChanged();


    }

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }

    }


    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    @Override
    public void onBackPressed() {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(SoilTestActivity.this);
        final MaterialDialog dialog = builder.build();
        builder.title(R.string.go_back).content(R.string.confirm_back).positiveText(R.string.yes).negativeText(R.string.cancel).typeface("roboto_bold.ttf", "roboto_light.ttf");
        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
                try {
                    startActivity(new Intent(SoilTestActivity.this, FarmerProfileActivity.class));
                    finish();
                } catch (Exception e) {
                    Snackbar.show(SoilTestActivity.this, e.toString());
                    e.printStackTrace();
                }
            }
        });
        builder.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog materialDialog, DialogAction which) {
                dialog.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}