package com.tcs.farmss.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;

import com.tcs.farmss.R;
import com.tcs.farmss.database.DBActivity;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;
import com.tcs.farmss.ui.button.ButtonPlus;

import butterknife.Bind;
import butterknife.ButterKnife;
import fr.ganfra.materialspinner.MaterialSpinner;

/**
 * Created by Harsh on 1/20/2017.
 */
public class AddFarmerActivity extends AppCompatActivity {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.spinnerState)
    MaterialSpinner spinnerState;
    @Bind(R.id.spinnerDistrict)
    MaterialSpinner spinnerDistrict;
    @Bind(R.id.spinnerTehsil)
    MaterialSpinner spinnerTehsil;
    @Bind(R.id.btnAddFarmer)
    ButtonPlus btnAddFarmer;
    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.etMobileReg)
    EditText etMobileReg;
    @Bind(R.id.etPinCode)
    EditText etPinCode;
    @Bind(R.id.etVillage)
    EditText etVillage;
    @Bind(R.id.checkboxTerms)
    CheckBox checkboxTerms;
    private String[] state;
    private String[] district;
    private String[] tehsil;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_farmer);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.add_new)));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnAddFarmer.setBackgroundResource(R.drawable.ripple);
        }

        state = getApplication().getResources().getStringArray(R.array.state);
        district = getApplication().getResources().getStringArray(R.array.district);
        tehsil = getApplication().getResources().getStringArray(R.array.tehsil);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddFarmerActivity.this, android.R.layout.simple_spinner_item, state);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerState.setAdapter(adapter);

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(AddFarmerActivity.this, android.R.layout.simple_spinner_item, district);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDistrict.setAdapter(adapter1);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(AddFarmerActivity.this, android.R.layout.simple_spinner_item, tehsil);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerTehsil.setAdapter(adapter2);

        final String address = spinnerTehsil.getSelectedItem().toString() + spinnerDistrict.getSelectedItem().toString() +
                spinnerState.getSelectedItem().toString();

        btnAddFarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etName.getText().toString().equals("")) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_name_error));
                    return;
                }
                if (etMobileReg.getText().toString().equals("") || !(etMobileReg.getText().length() == 10)) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_mobile_error));
                    return;
                }
                if (spinnerState.getSelectedItem().toString().equals(getString(R.string.select_State))) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_state_error));
                    return;
                }
                if (spinnerDistrict.getSelectedItem().toString().equals(getString(R.string.select_District))) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_district_error));
                    return;
                }
                if (etVillage.getText().toString().equals("")) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_village_error));
                    return;
                }
                if (etPinCode.getText().toString().equals("") || !(etPinCode.getText().length() == 6)) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.farmer_pincode_error));
                    return;
                }
                if (!checkboxTerms.isChecked()) {
                    Snackbar.show(AddFarmerActivity.this, getString(R.string.terms_and_conditions__error));
                    return;
                } else {
                    DBActivity db = new DBActivity(AddFarmerActivity.this);
                    db.open();
                    db.insertDB(etName.getText().toString(), etMobileReg.getText().toString(),
                            address, etVillage.getText().toString(), etPinCode.getText().toString());
                    Snackbar.success(AddFarmerActivity.this, "Successfully Added");
                    db.close();
                    startActivity(new Intent(AddFarmerActivity.this, DashboardActivity.class));
                    finish();
                }
            }
        });
    }
}
