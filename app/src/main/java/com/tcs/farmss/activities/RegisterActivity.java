package com.tcs.farmss.activities;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;

import com.tcs.farmss.R;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.button.ButtonPlus;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/19/2017.
 */
public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.etName)
    EditText etName;
    @Bind(R.id.etMobileReg)
    EditText etMobileReg;
    @Bind(R.id.etPasswordReg)
    EditText etPasswordReg;
    @Bind(R.id.etConfirmPasswordReg)
    EditText etConfirmPasswordReg;
    @Bind(R.id.spinnerSelectState)
    Spinner spinnerSelectState;
    @Bind(R.id.spinnerSelectDistrict)
    Spinner spinnerSelectDistrict;
    @Bind(R.id.spinnerSelectTehsil)
    Spinner spinnerSelectTehsil;
    @Bind(R.id.btnRegister)
    ButtonPlus btnRegister;
    @Bind(R.id.checkboxTerms)
    CheckBox checkboxTerms;
    @Bind(R.id.etVillage)
    EditText etVillage;
    @Bind(R.id.etPinCode)
    EditText etPinCode;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.RegisterActivityLable)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            btnRegister.setBackgroundResource(R.drawable.ripple);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }
}
