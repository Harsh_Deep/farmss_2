package com.tcs.farmss.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.tcs.farmss.R;
import com.tcs.farmss.fragments.DashboardFragment;
import com.tcs.farmss.fragments.MyProfileFragment;
import com.tcs.farmss.fragments.TermsAndConditionsFragment;
import com.tcs.farmss.ui.CustomTitle;
import com.tcs.farmss.ui.Snackbar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/20/2017.
 */
public class DashboardActivity extends AppCompatActivity {


    @Bind(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @Bind(R.id.navigation_view)
    NavigationView navigationView;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private MenuItem previousMenuItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.app_name)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_menu);

        //  navigationView.inflateMenu(R.menu.menu_drawer);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (previousMenuItem != null) {
                    previousMenuItem.setChecked(false);
                }
                item.setCheckable(true);
                item.setChecked(true);
                drawerLayout.closeDrawers();

                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()) {
                    case R.id.dashboardHome:
                        DashboardFragment dashboardFragment = new DashboardFragment();
                        fragmentTransaction.replace(R.id.frame, dashboardFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle(CustomTitle.getTitle(DashboardActivity.this, getResources().getString(R.string.app_name)));
                        return true;

                    case R.id.myProfile:
                        MyProfileFragment myProfileFragment = new MyProfileFragment();
                        fragmentTransaction.replace(R.id.frame, myProfileFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle(CustomTitle.getTitle(DashboardActivity.this, getResources().getString(R.string.my_profile)));
                        return true;

                    case R.id.terms:
                        TermsAndConditionsFragment termsAndConditionsFragment = new TermsAndConditionsFragment();
                        fragmentTransaction.replace(R.id.frame, termsAndConditionsFragment);
                        fragmentTransaction.commit();
                        getSupportActionBar().setTitle(CustomTitle.getTitle(DashboardActivity.this, getResources().getString(R.string.terms_conditions)));
                        return true;

                    case R.id.logout:
                        MaterialDialog.Builder builder = new MaterialDialog.Builder(DashboardActivity.this);
                        final MaterialDialog dialog = builder.build();
                        builder.title(R.string.logout).content(R.string.logout_message).positiveText(R.string.logout).negativeText(R.string.cancel).typeface("roboto_bold.ttf", "roboto_light.ttf");
                        builder.onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction which) {
                                dialog.dismiss();
                                try {
                                    startActivity(new Intent(DashboardActivity.this, LoginActivity.class));
                                    ActivityCompat.finishAffinity(DashboardActivity.this);
                                } catch (Exception e) {
                                    Snackbar.show(DashboardActivity.this, e.toString());
                                    e.printStackTrace();
                                }
                            }
                        });
                        builder.onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(MaterialDialog materialDialog, DialogAction which) {
                                dialog.dismiss();
                            }
                        });
                        builder.show();
                        return true;

                    default:
                        return true;
                }
            }

        });


        //inflating the dashboard fragment
        DashboardFragment fragment = new DashboardFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.frame, fragment);
        fragmentTransaction.commit();


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.openDrawer, R.string.closeDrawer) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_drawer, menu);
        return true;
    }
*/
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame);
        if (f instanceof DashboardFragment)
            super.onBackPressed();
        else {
            Intent intent = new Intent(DashboardActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }

    }
}

