package com.tcs.farmss.activities;

import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;

import com.tcs.farmss.R;
import com.tcs.farmss.adapters.CropCardAdapter;
import com.tcs.farmss.constants.CropDTO;
import com.tcs.farmss.ui.CustomTitle;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Harsh on 1/25/2017.
 */
public class CropSelectionActivity extends AppCompatActivity {

    @Bind(R.id.recyclerCrops)
    RecyclerView recyclerCrops;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    private CropCardAdapter adapter;
    private List<CropDTO> cropDTOs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.acitvity_crop_selection);
        populate();
    }

    private void populate() {
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(CustomTitle.getTitle(this, getResources().getString(R.string.Select_Crop)));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        cropDTOs = new ArrayList<>();
        adapter = new CropCardAdapter(this, cropDTOs);

        RecyclerView.LayoutManager manager = new GridLayoutManager(this, 2);
        recyclerCrops.setLayoutManager(manager);
        recyclerCrops.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerCrops.setItemAnimator(new DefaultItemAnimator());
        recyclerCrops.setAdapter(adapter);
        prepareCrops();
    }

    private void prepareCrops() {
        int[] icons = {R.mipmap.ic_launcher, R.mipmap.ic_launcher, R.mipmap.ic_launcher};

        CropDTO a = new CropDTO("Cabbage", icons[0]);
        cropDTOs.add(a);

        CropDTO b = new CropDTO("Onion", icons[1]);
        cropDTOs.add(b);

        CropDTO c = new CropDTO("Tomato", icons[2]);
        cropDTOs.add(c);

        CropDTO d = new CropDTO("Chilli", icons[0]);
        cropDTOs.add(d);

        CropDTO e = new CropDTO("Grapes", icons[0]);
        cropDTOs.add(e);

        CropDTO f = new CropDTO("Others", icons[0]);
        cropDTOs.add(f);

        adapter.notifyDataSetChanged();
    }


    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }

    }


    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }


}

