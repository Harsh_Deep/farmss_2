package com.tcs.farmss.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tcs.farmss.R;
import com.tcs.farmss.adapters.ProfileAdapter;
import com.tcs.farmss.constants.ProfileInfo;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyProfileFragment extends Fragment {

    private View parentView;
    private ProfileAdapter adapter;
    @Bind(R.id.profileRecyclerView)
    RecyclerView profileRecyclerView;


    public MyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView =  inflater.inflate(R.layout.fragment_my_profile, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        adapter = new ProfileAdapter(getActivity(), getData());
        profileRecyclerView.setAdapter(adapter);
        profileRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    public static List<ProfileInfo> getData() {
        List<ProfileInfo> data = new ArrayList<>();
        String[] attributes = {"Name", "Phone No.", "Email", "Address", "City", "PIN", "Work Location"};
        String[] values = {"Harsh Deep Singh", "7895052263", "harshdeep18894@gmail.com", "L.L.R.M Medical College", "Meerut", "250004", "Nasik"};

        for (int i = 0; i < attributes.length && i < values.length; i++) {
            ProfileInfo current = new ProfileInfo();
            current.attribute = attributes[i];
            current.value = values[i];
            data.add(current);
        }
        return data;
    }


}
