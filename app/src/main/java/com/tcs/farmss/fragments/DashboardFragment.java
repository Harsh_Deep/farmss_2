package com.tcs.farmss.fragments;


import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.tcs.farmss.R;
import com.tcs.farmss.activities.AddFarmerActivity;
import com.tcs.farmss.activities.DashboardActivity;
import com.tcs.farmss.activities.FarmerProfileActivity;
import com.tcs.farmss.adapters.DashboardListAdapter;
import com.tcs.farmss.database.DBActivity;
import com.tcs.farmss.ui.Snackbar;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class DashboardFragment extends Fragment {


    @Bind(R.id.fabAddFarmer)
    FloatingActionButton fabAddFarmer;
    @Bind(R.id.listFarmers)
    ListView listFarmers;
    DBActivity db;
    Cursor cursor;
    private DashboardListAdapter adapter;
    private View parentView;

    public DashboardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        parentView = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, parentView);
        populate();
        return parentView;
    }

    private void populate() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            fabAddFarmer.setBackgroundResource(R.drawable.ripple);
        }

        fabAddFarmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AddFarmerActivity.class));
            }
        });

        db = new DBActivity(getActivity());
        db.open();
        cursor = db.getData();
        db.close();
        if (cursor == null){
            Snackbar.show(getActivity(), "No Data is Present.. Add Some to View.");
        }
        adapter = new DashboardListAdapter(getActivity(), cursor, 0);
        listFarmers.setAdapter(adapter);

        listFarmers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(getActivity(), FarmerProfileActivity.class));
            }
        });
    }
}
