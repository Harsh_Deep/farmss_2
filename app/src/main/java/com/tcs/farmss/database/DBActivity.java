package com.tcs.farmss.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by Harsh on 1/23/2017.
 */
public class DBActivity {

    private static final String DATABASE_NAME = "farmss";
    private static final String DATABASE_TABLE = "farmers";
    private static final int DATABASE_VERSION = 1;
    public static final String KEY_ROWID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_MOBILE = "mobile";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_VILLAGE = "village";
    public static final String KEY_CODE = "pincode";
    private DBHelper dbHelper;
    private final Context myContext;
    private SQLiteDatabase myDataBase;

    class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + DATABASE_TABLE +
                    "(" + KEY_ROWID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + KEY_NAME + " VARCHAR(100), "
                    + KEY_MOBILE + " VARCHAR(100), "
                    + KEY_ADDRESS + " VARCHAR(100), "
                    + KEY_VILLAGE + " VARCHAR(100), "
                    + KEY_CODE + " VARCHAR(100));");
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE);
            onCreate(db);
        }

    }

    public DBActivity(Context c) {
        myContext = c;
    }

    public void close() {
        dbHelper.close();
    }

    public DBActivity open() {
        dbHelper = new DBHelper(myContext);
        myDataBase = dbHelper.getWritableDatabase();
        return this;
    }

    public void insertDB(String name, String mobile, String address, String village, String code) {
        ContentValues cv = new ContentValues();
        cv.put(KEY_NAME, name);
        cv.put(KEY_MOBILE, mobile);
        cv.put(KEY_ADDRESS, address);
        cv.put(KEY_VILLAGE, village);
        cv.put(KEY_CODE, code);
        myDataBase.insert(DATABASE_TABLE, null, cv);
    }

    public Cursor getData() {
        //   myDataBase = dbHelper.getReadableDatabase();

        String selectQuery = "SELECT  rowid as " +
                KEY_ROWID + "," +
                KEY_NAME + "," +
                KEY_MOBILE + "," +
                KEY_ADDRESS + "," +
                KEY_VILLAGE + "," +
                KEY_CODE +
                " FROM " + DATABASE_TABLE;

        Cursor cursor = myDataBase.rawQuery(selectQuery, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }

    /*public Cursor getDataByPlatform(String search) {
        String selectQuery = "SELECT  rowid as " +
                KEY_ROWID + "," +
                KEY_PLATFORM + "," +
                KEY_USERNAME + "," +
                KEY_PASSWORD +
                " FROM " + DATABASE_TABLE +
                " WHERE " + KEY_PLATFORM + "  LIKE  '%" + search + "%' ";
        Cursor cursor = myDataBase.rawQuery(selectQuery, null);

        if (cursor == null) {
            return null;
        } else if (!cursor.moveToFirst()) {
            cursor.close();
            return null;
        }
        return cursor;
    }
*/
/*

    public ArrayList<String> getPasswords() {
        ArrayList<String> passwords = new ArrayList<>();
        String query = "select * from " + DATABASE_TABLE;
        Cursor c = myDataBase.rawQuery(query, null);
        int iPassword = c.getColumnIndex(KEY_PASSWORD);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            passwords.add(c.getString(iPassword));
        }
        return passwords;
    }
*/

}
