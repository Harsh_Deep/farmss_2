package com.tcs.farmss.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.tcs.farmss.R;
import com.tcs.farmss.activities.CameraActivity;
import com.tcs.farmss.constants.TestDTO;

import java.util.List;

/**
 * Created by Harsh on 1/20/2017.
 */
public class SoilTestAdapter extends RecyclerView.Adapter<SoilTestAdapter.MyViewHolder> {

    private Context context;
    private List<TestDTO> testList;

    public SoilTestAdapter(Context context, List<TestDTO> testList) {
        this.context = context;
        this.testList = testList;
    }

    public void hide(final int position) {
        new MaterialDialog.Builder(context)
                .title(R.string.test_confirmation)
                .content(R.string.confirm_test_message)
                .positiveText(R.string.yes)
                .negativeText(R.string.cancel)
                .typeface("roboto_bold.ttf", "roboto_light.ttf")
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                        try {
                            testList.remove(position);
                            notifyItemRemoved(position);
                            context.startActivity(new Intent(context, CameraActivity.class));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }).onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                dialog.dismiss();
            }
        }).show();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.test_card, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SoilTestAdapter.MyViewHolder holder, final int position) {
        TestDTO testDTO = testList.get(position);
        holder.title.setText(testDTO.getName());

        Glide.with(context).load(testDTO.getThumbnail()).into(holder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txtTitle);
            thumbnail = (ImageView) view.findViewById(R.id.imgThumbnail);
            thumbnail.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            hide(getPosition());
        }
    }
}


