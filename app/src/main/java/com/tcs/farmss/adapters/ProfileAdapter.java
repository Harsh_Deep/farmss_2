package com.tcs.farmss.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tcs.farmss.R;
import com.tcs.farmss.constants.ProfileInfo;

import java.util.Collections;
import java.util.List;

/**
 * Created by Harsh on 1/20/2017.
 */
public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ProfileViewHolder>{



    private LayoutInflater inflater;
    List<ProfileInfo> data = Collections.emptyList();



    public ProfileAdapter(Context context, List<ProfileInfo> data) {
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public ProfileAdapter.ProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.profile_custom_row, parent, false);
        ProfileViewHolder holder = new ProfileViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ProfileAdapter.ProfileViewHolder holder, int position) {
        ProfileInfo info = data.get(position);
        holder.txtAttribute.setText(info.attribute);
        holder.txtValue.setText(info.value);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class ProfileViewHolder extends RecyclerView.ViewHolder {

        TextView txtAttribute;
        TextView txtValue;

        public ProfileViewHolder(View itemView) {
            super(itemView);

            txtAttribute = (TextView) itemView.findViewById(R.id.txtAttribute);
            txtValue = (TextView) itemView.findViewById(R.id.txtValue);

        }

    }
}
