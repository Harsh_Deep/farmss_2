package com.tcs.farmss.adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.tcs.farmss.R;
import com.tcs.farmss.database.DBActivity;

/**
 * Created by Harsh on 1/23/2017.
 */
public class DashboardListAdapter extends CursorAdapter {

    TextView txtDashboardNameValue, txtDashboardMobileValue,
            txtDashboardAddressValue, txtDashboardVillageValue,
            txtDashboardPinCodeValue;
    private LayoutInflater inflater;

    public DashboardListAdapter(Context context, Cursor c, int flags){
        super(context, c, flags);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        View view = inflater.inflate(R.layout.dashboard_list_custom_row, parent, false);
        ViewHolder holder = new ViewHolder();
        holder.txtDashboardNameValue = (TextView) view.findViewById(R.id.txtDashboardNameValue);
    /*    holder.txtDashboardMobileValue = (TextView) view.findViewById(R.id.txtDashboardMobileValue);
        holder.txtDashboardAddressValue = (TextView) view.findViewById(R.id.txtDashboardAddressValue);
    */    holder.txtDashboardVillageValue = (TextView) view.findViewById(R.id.txtDashboardVillageValue);
    //    holder.txtDashboardPinCodeValue = (TextView) view.findViewById(R.id.txtDashboardPinCodeValue);
        view.setTag(holder);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder holder = (ViewHolder) view.getTag();
        holder.txtDashboardNameValue.setText(cursor.getString(cursor.getColumnIndex(DBActivity.KEY_NAME)));
    /*    holder.txtDashboardMobileValue.setText(cursor.getString(cursor.getColumnIndex(DBActivity.KEY_MOBILE)));
        holder.txtDashboardAddressValue.setText(cursor.getString(cursor.getColumnIndex(DBActivity.KEY_ADDRESS)));
    */    holder.txtDashboardVillageValue.setText(cursor.getString(cursor.getColumnIndex(DBActivity.KEY_VILLAGE)));
    //    holder.txtDashboardPinCodeValue.setText(cursor.getString(cursor.getColumnIndex(DBActivity.KEY_CODE)));
    }

    static class ViewHolder {
        TextView txtDashboardNameValue;
    /*    TextView txtDashboardMobileValue;
        TextView txtDashboardAddressValue;
    */    TextView txtDashboardVillageValue;
        //   TextView txtDashboardPinCodeValue;
    }
}
